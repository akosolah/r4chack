angular.module('AUTISME', [])
	.controller('AutismCtrl', function ($scope, $timeout) {
		//Initialize page with home
		$scope.page = 'partials/home.html';
		$scope.openPage = function (page) {
			$scope.page = 'partials/' + page + '.html';
		};
		$timeout(function () {
			console.log($scope.page);
			if($scope.page === 'partials/home.html') {
				$scope.page = 'partials/main.html';
			}
		}, 3000);
	})
	.directive('chartinterest', function () {
		var chartData = {
			type: 'bar',
			data: {
				labels: ["Colors Interested"],
				datasets: [{
						label: "Green",
						data: [7],
						backgroundColor: "green"
			}, {
						label: "Yellow",
						data: [10],
						backgroundColor: "yellow"
			},
					{
						label: "Blue",
						data: [24],
						backgroundColor: "blue"
			}, {
						label: "Orange",
						data: [3],
						backgroundColor: "orange"
			},
					{
						label: "Pink",
						data: [6],
						backgroundColor: "pink"
			},
					{
						label: "Black",
						data: [4],
						backgroundColor: "black"
			},
					{
						label: "Red",
						data: [3],
						backgroundColor: "red"
			}]
			},
			options: {
				legend: {
					display: false,
					position: 'left',
					labels: {
						boxWidth: 12
					}
				},
				scales: {
					xAxes: [{
						display: false,
					}],
					yAxes: [{
						display: false,
						ticks: {
							beginAtZero: true
						}
					}],
				}
			}
		};
		return {
			restrict: 'E',
			replace: true,
			scope: {},
			template: '<div><canvas></canvas></div>',
			link: function (scope, element, attrib) {
				scope.chart = new Chart(element.children(0)[0], angular.copy(chartData));
				scope.chart.chart.aspectRatio = 2.5;
			}
		};
	});
